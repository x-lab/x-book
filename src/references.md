# Appendix A - References

**Rust**

1. [Rust book](https://doc.rust-lang.org/book)
2. [Rust reference](https://doc.rust-lang.org/nightly/reference)
3. [Rustonomicon](https://doc.rust-lang.org/nomicon)
4. [Rust by example](https://doc.rust-lang.org/nightly/rust-by-example)
5. [Rust build system](https://doc.rust-lang.org/cargo)
6. [Rustdoc](https://doc.rust-lang.org/rustdoc)
7. [Rustlings](https://github.com/rust-lang/rustlings)
8. [Rust-C++ Interop](https://cxx.rs/index.html)

**Qml**

1. [QML Coding Guide](https://github.com/Furkanzmc/QML-Coding-Guide/blob/master/README.md)
