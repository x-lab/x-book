# Getting started

## Rust

Install `Rust`:
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Qt

Install `Qt`.

## Prepare environment

```bash
## #############################################################################
## X labs
## #############################################################################

export QT_DIR=/Users/jwintz/Development/qt/6.2.0/macos
export QT_INCLUDE_PATH=$QT_DIR/include
export QT_LIBRARY_PATH=$QT_DIR/lib
export QTDIR=$QT_DIR

export DEP_QT_INCLUDE_PATH=$QT_INCLUDE_PATH
export DEP_QT_LIBRARY_PATH=$QT_LIBRARY_PATH
export DEP_QT_VERSION=6.2.0

export RUSTFLAGS="-C link-args=-Wl,-rpath,$DEP_QT_LIBRARY_PATH"
```
