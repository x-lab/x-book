# Summary

- [Getting started](./getting_started.md)
- [Crate - x-logger](./x-logger.md)
- [Crate - x-gui](./x-gui.md)
- [Crate - x-quick](./x-quick.md)
- [Appendix A - Conventions - QML](./conventions.md)
- [Appendix B - References](./references.md)
